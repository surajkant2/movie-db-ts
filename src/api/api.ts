import axios, { AxiosResponse } from "axios";
import { LoginDetailsType, LoginReponseType, MovieDetailType, MoviesResponseType, VideosResponseType } from "../types";

const api = axios.create({
  baseURL: "https://api.themoviedb.org/3",
  params: {
    api_key: process.env.REACT_APP_API_KEY,
  },
});

export const imageUrl:((path:string, size?:string)=>string) = (path, size = "original") => {
  return `https://image.tmdb.org/t/p/${size}${path}`;
};

export const getMovies:((page:number, query:string)=>Promise<AxiosResponse<MoviesResponseType, any>>) = (page, query) => {
  if (query) {
    return api.get<MoviesResponseType>("/search/movie", {
      params: {
        query: query,
        page: page,
      },
    });
  } else {
    return api.get<MoviesResponseType>("/trending/movie/week", {
      params: {
        page: page,
      },
    });
  }
};

export const getMovieDetails:((movieId:string)=>Promise<AxiosResponse<MovieDetailType, any>>) = (movieId) => {
  return api.get<MovieDetailType>(`/movie/${movieId}`);
};

export const getMovieTrailerTeaser:((movieId:string)=>Promise<AxiosResponse<VideosResponseType, any>>) = (movieId) => {
  return api.get<VideosResponseType>(`/movie/${movieId}/videos`);
};

export const getRequestToken:(()=>Promise<AxiosResponse<LoginReponseType, any>>) = () => {
  return api.get<LoginReponseType>(`/authentication/token/new`);
};

export const login:((loginDetails:LoginDetailsType) => Promise<AxiosResponse<LoginReponseType>>) = (loginDetails) => {
  return api.post<LoginReponseType>(`/authentication/token/validate_with_login`, loginDetails);
};

export const isLoggedIn:(()=>boolean) = () => {
  const tokenDetails = JSON.parse(localStorage.getItem("movie_db") as string);
  if (
    tokenDetails?.token &&
    tokenDetails?.expires_at &&
    new Date(Date.now()) < new Date(tokenDetails?.expires_at)
  ) {
    return true;
  } else {
    localStorage.removeItem("movie_db");
    return false;
  }
};

export const setTokenToLocal:((token:string, expires_at:string)=>void) = (token, expires_at) => {
  localStorage.setItem(
    "movie_db",
    JSON.stringify({
      token,
      expires_at,
    })
  );
};

export const logout:(()=>void) = () => {
  localStorage.removeItem("movie_db");
};
