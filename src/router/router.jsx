import { createBrowserRouter } from "react-router-dom";
import App from '../App';
import Login from "../components/Login";
import Movies from "../components/Movies";
import MovieDetails from "../components/MovieDetails";

const router = createBrowserRouter([
    {
        path: "/",
        element: <App />,
        children: [
            {
                path: '/',
                element: <Login />
            },
            {
                path: 'movies',
                element: <Movies />
            },
            {
                path: 'movie-details/:movieId',
                element: <MovieDetails />
            }
        ],
    },
]);

export default router;