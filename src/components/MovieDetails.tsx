import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  getMovieDetails,
  getMovieTrailerTeaser,
  imageUrl,
  isLoggedIn,
} from "../api/api";
import arrowLeft from "../images/arrow-left-icon.svg";
import playIconBig from "../images/play-icon-big.svg";
import VideoModal from "./VideoModal";
import Loader from "./Loader";
import Image from "./Image";
import dummyImage from "../images/dummy-image.svg";
import { MovieDetailType } from "../types";

const MovieDetails = () => {
  const [movieDetails, setMovieDetails] = useState<MovieDetailType | null>(null);
  const [showVideoModal, setShowVideoModal] = useState<boolean>(false);
  const [videoId, setVideoId] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const { movieId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    if (!isLoggedIn()) {
      navigate("/");
    }
  });

  useEffect(() => {
    (async () => {
      setLoading(true);
      const responseMovie = await getMovieDetails(movieId || '');
      const responseVideos = await getMovieTrailerTeaser(movieId || '');
      setLoading(false);
      setVideoId(
        responseVideos.data.results.find(
          (item) => item.type === "Trailer" || item.type === "Teaser"
        )?.key || ""
      );
      setMovieDetails(responseMovie.data );
    })();
  }, [movieId]);

  const handleCloseVideoModal = () => {
    setShowVideoModal(false);
  };

  return (
    <div className="MovieDetails">
      {loading ? (
        <Loader />
      ) : (
        movieDetails && (
          <div
            className="movie-details-master"
            style={{
              backgroundImage: `linear-gradient(90deg, #000 30%, #0000 100%)${
                movieDetails.backdrop_path
                  ? `, url(${imageUrl(movieDetails.backdrop_path)})`
                  : ""
              }`,
            }}
          >
            <div className="movie-backdrop-mobile-wrap">
              <Image
                src={
                  movieDetails.backdrop_path
                    ? imageUrl(movieDetails.backdrop_path)
                    : dummyImage
                }
                alt={movieDetails.title}
                className={"movie-backdrop-mobile"}
              />
              <button className="back-button" onClick={() => navigate(-1)}>
                <img src={arrowLeft} alt="Go to previous page" />
              </button>
            </div>

            <div className="container">
              <div className="movie-details-wrap">
                <div className="flex f-aic">
                  <div className="left">
                    <button
                      className="back-button"
                      onClick={() => navigate(-1)}
                    >
                      <img src={arrowLeft} alt="Go to previous page" />
                    </button>
                    <div className="details">
                      <h1>{movieDetails.title}</h1>
                      <p className="rating">
                        Rating:{" "}
                        {Math.trunc((movieDetails.vote_average / 2) * 10) / 10}
                        /5
                      </p>
                      <p className="summary">{movieDetails.overview}</p>
                    </div>
                    <table>
                      <tbody>
                        <tr>
                          <th scope="row">Release Date</th>
                          <td>{movieDetails.release_date}</td>
                        </tr>
                        <tr>
                          <th scope="row">Original Language</th>
                          <td>
                            {movieDetails.spoken_languages
                              .map((l) => l.english_name)
                              .join(", ")}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className="right">
                    {videoId && (
                      <button onClick={() => setShowVideoModal(true)}>
                        <img src={playIconBig} alt="play trailer" />
                      </button>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
      )}
      {showVideoModal && (
        <VideoModal videoId={videoId} handleClose={handleCloseVideoModal} />
      )}
    </div>
  );
};

export default MovieDetails;
