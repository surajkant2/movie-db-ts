import banner from "../images/banner.jpg";
import MovieCard from "./MovieCard";
import Pagination from "./Pagination";
import { useOutletContext } from "react-router-dom";
import Loader from "./Loader";
import Image from "./Image";
import { OutletReturnType } from "../types";

const Movies = () => {
  const { movies, loadingMovies, totalPages, totalResults, page, query, handlePageChange } = useOutletContext<OutletReturnType>();

  return (
    <div className="Movies">
      <Image className="banner" src={banner} alt="Page Banner" />

      <div className="movies-wrapper">
        <div className="container">
          <h1>{query ? `Search results for: ${query}` : "Trending"}</h1>

          {loadingMovies ? (
            <Loader />
          ) : (
            <>
              {totalResults === 0 ? (
                <p className="gen-text">No result found</p>
              ) : (<>
                <div className="row">
                  {movies.map((movie) => (
                    <div className="col-3" key={movie.id}>
                      <MovieCard movie={movie} />
                    </div>
                  ))}
                </div>

                <Pagination
                  totalPages={totalPages}
                  page={page}
                  handlePageChange={handlePageChange}
                />
              </>)}
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Movies;
