import star from '../images/star-icon.svg';
import styles from '../styles/Rating.module.css'

interface RatingType {
    rating:number;
    total:number;
}

const Rating:React.FC<RatingType> = ({rating, total}) => {
    const int = parseInt(String(rating));
    const fraction = rating - int;
    const stars = [];

    for(let i = 0; i < int; i++){
        stars.push(<div key={i} style={{
            width: 15,
            height: 15,
            marginRight: 5,
            backgroundImage: `url(${star})`,
            backgroundPosition: 'left center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 15,
        }}></div>)
    }

    if(fraction){
        stars.push(<div key={'f'} style={{
            width: 15 * fraction,
            height: 15,
            marginRight: 5,
            backgroundImage: `url(${star})`,
            backgroundPosition: 'left center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 15,
        }}></div>)
    }

    return (<div className={styles.Rating}>
        <div className={styles.starsWrap}>{stars}</div>
        <div
            style={{
                fontFamily: "'Poppins', sans-serif",
                fontSize: 14,
                marginLeft: rating ? 7 : 0,
                color: "#fff",
            }}
            className="rating-text"
        >{rating} / {total}</div>
    </div>);
}
 
export default Rating;