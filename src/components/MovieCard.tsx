import { imageUrl } from "../api/api";
import Image from "./Image";
import Rating from "./Rating";
import playIcon from '../images/play-icon.svg';
import dummyImage from '../images/dummy-image.svg';
import { Link } from "react-router-dom";
import { MovieType } from "../types";

const MovieCard:React.FC<{movie:MovieType}> = ({movie}) => {
    return (
        <Link to={`/movie-details/${movie.id}`} className="MovieCard">
            <Image
                src={movie.backdrop_path ? imageUrl(movie.backdrop_path, 'w500') : dummyImage}
                alt={movie.title}
                ratio='500:281'
                className='movie-thumbnail'
            />
            <div className="movie-details">
                <div className="title-and-rating">
                    <h2 title={movie.title}>{movie.title}</h2>
                    <Rating rating={Math.round(movie.vote_average/2*10)/10} total={5} />
                </div>
                <img  className="play-button" src={playIcon} alt="" />
            </div>
        </Link>
    );
}
 
export default MovieCard;