const ButtonLoader:React.FC = () => {
  return (
    <div className="button-loader-wrap">
      <div className="button-loader"></div>
    </div>
  );
};

export default ButtonLoader;
