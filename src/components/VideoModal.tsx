import closeIcon from '../images/close-icon.svg';

interface VideoModalType {
    videoId: string;
    handleClose: ()=>void;
}
const VideoModal:React.FC<VideoModalType> = ({videoId, handleClose}) => {
    return (
        <div className="VideoModal">
            <div className="modal-wrap">
                <div className="header">
                    <button className="close-button" onClick={handleClose}>
                        <img src={closeIcon} alt="close" />
                    </button>
                </div>
                <div className="player-wrap">
                    <iframe
                        className="player"
                        src={`https://www.youtube.com/embed/${videoId}`}
                        title="YouTube video player"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowFullScreen
                    ></iframe>
                </div>
            </div>
        </div>
    );
}
export default VideoModal;