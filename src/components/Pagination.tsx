import preIcon from "../images/caret-left.svg";
import nextIcon from "../images/caret-right.svg";
import styles from '../styles/Pagination.module.css';

interface PaginationPropsType {
    totalPages:number;
    page:number;
    handlePageChange:(page:number)=>void
}

const Pagination:React.FC<PaginationPropsType> = ({totalPages, page, handlePageChange}) => {
    console.log(totalPages);
    if(totalPages === 1) {
        return null;
    }
    
    function getPagination(){
        const pageLinks = [];
        let from = 0;
        let to = 0;

        if (page < 3) {
            from = 1;
            to = totalPages > 5 ? 5 : totalPages;
        } else if (page > totalPages - 2) {
            from = totalPages - 4 < 1 ? 1 : totalPages - 4;
            to = totalPages;
        } else {
            from = page - 2 < 1 ? 1 : page - 2;
            to = page + 2 > totalPages ? totalPages : page + 2;
        }
    
        if(page !== 1){
            pageLinks.push(<button onClick={() => handlePageChange(page - 1)} className={styles.preNext} key="pre">
                <img src={preIcon} alt="Previous Page" />
            </button>);
        }
    
        if (page >= 4 && totalPages > 5) {
            pageLinks.push(
              <button onClick={() => handlePageChange(1)} key="1" className={styles.num}>
                1
              </button>
            );
        }
        
        if (page > 4 && totalPages > 6) {
            pageLinks.push(
              <span key="ellipsis-left" className={styles.ellipsis}>
                ...
              </span>
            );
        }
    
        for(let i = from; i <= to; i++){
            pageLinks.push(<button onClick={() => handlePageChange(i)} className={styles.num + (page === i ? " " + styles.active : "")} key={i}>{i}</button>);
        }
    
        if (page < totalPages - 3 && totalPages - 4 > 2) {
            pageLinks.push(
                <span key="ellipsis-right" className={styles.ellipsis}>
                  ...
                </span>
            );
        }

        if (page <= totalPages - 3 && totalPages - 3 > 2) {
            pageLinks.push(
              <button onClick={() => handlePageChange(totalPages)} key={totalPages} className={styles.num}>
                {totalPages}
              </button>
            );
        }
    
        if(page !== totalPages){
            pageLinks.push(<button onClick={() => handlePageChange(page + 1)} className={styles.preNext} key="next">
                <img src={nextIcon} alt="Next Page" />
            </button>);
        }

        return pageLinks;
    }

    console.log(getPagination());
    return (
        <div className={styles.Pagination}>
            {getPagination()}
        </div>
    );
}
 
export default Pagination;