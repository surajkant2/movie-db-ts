import { Outlet, useLocation, useNavigate, useSearchParams } from 'react-router-dom';
import './App.css';
import { useEffect, useState } from 'react';
import { getMovies, isLoggedIn } from './api/api';
import { MovieType } from './types';
import Navbar from './components/Navbar';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  const [loadingMovies, setLoadingMovies] = useState<boolean>(false) ;
  const [formValue, setFormValue] = useState<string>('');
  const [movies, setMovies] = useState<MovieType[]>([]);
  const [totalPages, setTotalPages] = useState<number>(0);
  const [totalResults, setTotalResults] = useState<number>(0);
  const [searchParams, setSearchParams] = useSearchParams();
  const [formValueChanged, setFormValueChanged] = useState<boolean>(false)
  const location = useLocation();
  const navigate = useNavigate();
  const isLoggedInStatic:boolean = isLoggedIn()

  const page = Number(searchParams.get('page')) || 1;
  const query = searchParams.get('query');

  useEffect(() => {
    if (!isLoggedInStatic && location.pathname !== '/') {
      navigate("/");
    } else if (isLoggedInStatic && location.pathname === '/') {
      navigate("/movies");
    }
  }, [location.pathname, isLoggedInStatic, navigate])

  useEffect(() => {
    if(formValueChanged){
      const id = setTimeout(() => {
        setSearchParams(pre => {
          if(!formValue){
            pre.delete('query');
          } else {
            pre.set('query', formValue)
          }
          pre.set('page', '1')
          setFormValueChanged(false)
          return pre;
        })
      }, 1000)
      return () => clearTimeout(id)
    } else if(!query) {
      setFormValue('');
    }
  }, [formValue, setSearchParams, formValueChanged, query]);

  useEffect(() => {
    (async () => {
      if(location.pathname === '/movies'){
        setLoadingMovies(true);
        const response = await getMovies(page, query || '');
        setLoadingMovies(false);
        setMovies(response.data.results);
        setTotalPages(response.data.total_pages);
        setTotalResults(response.data.total_results);
      }
    })();
  }, [page, query, location.pathname]);

  const handlePageChange = (page:number) => {
    setSearchParams(pre => {
      pre.set('page', String(page))
      return pre;
    })
  }

  const handleFormValueChange = (e:React.ChangeEvent<HTMLInputElement>) => {
    setLoadingMovies(true);
    setFormValueChanged(true)
    setFormValue(e.target.value)
    if(!e.target.value.trim() || e.target.value.trim() === query){
      setLoadingMovies(false);
    }
  }

  return (
    <div className="App">
      <Navbar
        formValue={formValue}
        handleFormValueChange={handleFormValueChange}
      />
      <Outlet context={{
        movies,
        loadingMovies,
        setLoadingMovies,
        totalResults,
        totalPages,
        page,
        handlePageChange,
        query,
      }} />
      <ToastContainer />
    </div>
  );
}

export default App;
