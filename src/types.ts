export interface MovieType {
    id: number;
    title: string;
    backdrop_path: string | null;
    vote_average: number;
    overview: string | null;
    release_date: string; 
}

export interface MovieDetailType extends MovieType {
    spoken_languages: {english_name: string, iso_639_1:string}[]
}

export interface LoginDetailsType {
    username: string;
    password: string;
    request_token: string;
}

export interface OutletReturnType {
    movies:MovieType[];
    loadingMovies:boolean; 
    totalPages:number;
    totalResults:number;
    page:number;
    query:string;
    handlePageChange:(page:number) => void;
}

export interface FormStateItemType {
    value: string;
    focusedOrNotEmpty: boolean;
}

export interface FormStateType {
    username: FormStateItemType;
    password: FormStateItemType;
}

export interface LoginReponseType {
    success: boolean;
    expires_at: string;
    request_token: string;
}

export interface LoginErrorType {
    status_message: string;
    status_code: number;
}

export interface MoviesResponseType {
    page: number,
    total_pages: number,
    total_results: number,
    results: MovieType[]
}

export interface VideosType {
    key: string;
    type: string;
}

export interface VideosResponseType {
    id: number;
    results: VideosType[];
}