import Rating from '../components/Rating';
import type { Meta, StoryObj } from '@storybook/react';

const meta:Meta<typeof Rating> = {
    component: Rating,
    render: args => <div style={{
        backgroundColor: '#000',
        padding: 15,
    }}><Rating {...args} /></div>,
}

export default meta;

type Story = StoryObj<typeof Rating>;

// export const Default:Story = { 
//     render: () => <Rating rating={0} total={5} />,
// };

// export const OneStar:Story = {
//     render: () => <Rating rating={1} total={5} />,
// };

// export const TwoStars:Story = {
//     render: () => <Rating rating={2} total={5} />,
// };

// export const ThreeStars:Story = {
//     render: () => <Rating rating={3} total={5} />,
// };

// export const FourStars:Story = {
//     render: () => <Rating rating={4} total={5} />,
// };

// export const FiveStars:Story = {
//     render: () => <Rating rating={5} total={5} />,
// };

// export const OnePointOne:Story = {
//     render: () => <Rating rating={1.1} total={5} />,
// };

// export const OnePointTwo:Story = {
//     render: () => <Rating rating={1.2} total={5} />,
// };

// export const OnePointThree:Story = {
//     render: () => <Rating rating={1.3} total={5} />,
// };

// export const OnePointFour:Story = {
//     render: () => <Rating rating={1.4} total={5} />,
// };

// export const OnePointFive:Story = {
//     render: () => <Rating rating={1.5} total={5} />,
// };

// export const OnePointSix:Story = {
//     render: () => <Rating rating={1.6} total={5} />,
// };

// export const OnePointSeven:Story = {
//     render: () => <Rating rating={1.7} total={5} />,
// };

// export const OnePointEight:Story = {
//     render: () => <Rating rating={1.8} total={5} />,
// };

// export const OnePointNine:Story = {
//     render: () => <Rating rating={1.9} total={5} />,
// };


// =============== using args


export const Default:Story = {
    args: {
        total: 5,
        rating: 0,
    },
};

export const OneStar:Story = {
    args: {
        ...Default.args,
        rating: 1,
    }
}

export const TwoStars:Story = {
    args: {
        ...Default.args,
        rating: 2,
    }
}

export const TwoPointFiveStars:Story = {
    args: {
        ...Default.args,
        rating: 2.5,
    }
}

export const TwoPointTwoFiveStars:Story = {
    args: {
        ...Default.args,
        rating: 2.25,
    }
}

export const TwoPointSevenFiveStars:Story = {
    args: {
        ...Default.args,
        rating: 2.75,
    }
}

export const FiveStars:Story = {
    args: {
        ...Default.args,
        rating: 5,
    }
}