import Pagination from '../components/Pagination';
import type { Meta, StoryObj } from '@storybook/react';

const meta:Meta<typeof Pagination> = {
    component: Pagination,
    argTypes: {
        totalPages: {
            control: {
                type: 'number',
                min: 1,
            }
        },
        page: {
            control: {
                type: 'number',
                min: 1,
            }
        },
    },
    render: args => <div style={{
        backgroundColor: '#000',
        padding: 15,
    }}><Pagination {...args} /></div>,
}

export default meta;

type Story = StoryObj<typeof Pagination>;

export const First:Story = {
    args: {
        totalPages:1000,
        page:1,
        handlePageChange:(page:number)=>{}
    },
};

export const Second:Story = {
    args: {
        totalPages:1000,
        page:2,
        handlePageChange:(page:number)=>{}
    },
};

export const Third:Story = {
    args: {
        totalPages:1000,
        page:3,
        handlePageChange:(page:number)=>{}
    },
};

export const Fourth:Story = {
    args: {
        totalPages:1000,
        page:4,
        handlePageChange:(page:number)=>{}
    },
};

export const Fifth:Story = {
    args: {
        totalPages:1000,
        page:5,
        handlePageChange:(page:number)=>{}
    },
};

export const Last:Story = {
    args: {
        totalPages:1000,
        page:1000,
        handlePageChange:(page:number)=>{}
    },
};

export const SecondLast:Story = {
    args: {
        totalPages:1000,
        page:999,
        handlePageChange:(page:number)=>{}
    },
};

export const ThirdLast:Story = {
    args: {
        totalPages:1000,
        page:998,
        handlePageChange:(page:number)=>{}
    },
};

export const FourthLast:Story = {
    args: {
        totalPages:1000,
        page:997,
        handlePageChange:(page:number)=>{}
    },
};

export const FifthLast:Story = {
    args: {
        totalPages:1000,
        page:996,
        handlePageChange:(page:number)=>{}
    },
};

export const LessThanFivePages:Story = {
    args: {
        totalPages:6,
        page:1,
        handlePageChange:(page:number)=>{}
    },
};